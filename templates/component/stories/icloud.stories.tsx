import {CapComponentName} from '../src';

export default {
    title: '{CapComponentName}',
    component: {CapComponentName},
};

export const Demo = () => {
    return (
        <{CapComponentName} />
    );
};
