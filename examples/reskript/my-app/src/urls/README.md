# URL目录

目录下放置系统中会使用的各种URL，每个URL对象是一个[uri-templates](https://github.com/geraintluff/uri-templates)创建的对象（`new UriTemplate(...)`）。

以模块为粒度进行组织，如`repo.js`中放置和代码库有关的内容。

在`index.js`中导出所有模块的内容。
