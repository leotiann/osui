# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.6.2...@osui/icloud-theme@0.6.3) (2020-10-21)


### Bug Fixes

* message closeicon 位置调整 ([4a7601b](https://gitee.com/gitee-fe/osui/tree/master/commits/4a7601b6249904d74ca649269ed9051eb3fb91bb))
* table icloud-theme样式修复 ([3f0d5e3](https://gitee.com/gitee-fe/osui/tree/master/commits/3f0d5e36838640cf65145b5f489dfbb629f0ac8f))





## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.6.1...@osui/icloud-theme@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.5.6...@osui/icloud-theme@0.6.1) (2020-10-19)


### Bug Fixes

* form修复onChange没有透传的问题 ([28665aa](https://gitee.com/gitee-fe/osui/tree/master/commits/28665aac36d94950b218f94a8dd9e1edcf55da96))
* modal icloud theme 调整 ([7c56388](https://gitee.com/gitee-fe/osui/tree/master/commits/7c563884ea60f84489c56aa018d67a8e892fc5f4))
* modal icloud theme 调整 ([c8e0598](https://gitee.com/gitee-fe/osui/tree/master/commits/c8e059806876a3154c23b629008a9e27cae01caa))
* modal, switch icloud-theme样式修复 ([d1eea7d](https://gitee.com/gitee-fe/osui/tree/master/commits/d1eea7d5984887a7bec2fd8fa59dcf867b6564c4))
* pagniation icloud主题调整 ([1d1f513](https://gitee.com/gitee-fe/osui/tree/master/commits/1d1f5139658689663e4cdfcfd843cbcd6b654ba6))
* popover icloud theme调整 ([ceaa52a](https://gitee.com/gitee-fe/osui/tree/master/commits/ceaa52ac3f5057b8f644938dd5e722c2676827ef))
* radio button hover color icloud theme 调整 ([a8ed354](https://gitee.com/gitee-fe/osui/tree/master/commits/a8ed354377eea758c0e239cca0618a5d91c0e8d6))
* radio调整icloud主题 ([86a94a3](https://gitee.com/gitee-fe/osui/tree/master/commits/86a94a359ff0a03301df779f240a9e6beaa5933e))
* switch icloud主题样式调整 ([4cfd4c3](https://gitee.com/gitee-fe/osui/tree/master/commits/4cfd4c345a704f1f97f0453b7a921395c1164ccc))
* tabs icloud theme 调整 ([a0815ba](https://gitee.com/gitee-fe/osui/tree/master/commits/a0815baa516af2c799ad982538868ec6fed88d9c))
* 增加button default border高亮, success, error, warning 状态 ([32dfd0f](https://gitee.com/gitee-fe/osui/tree/master/commits/32dfd0f8ef987a3e0a3adc724f75c07f5d6c9a2a))
* 调整checkbox icloud主题样式 ([fd43d5b](https://gitee.com/gitee-fe/osui/tree/master/commits/fd43d5b6c846e19db8682b6b7ef70392844029f9))


### Features

* form icloud theme调整 ([b90c6e4](https://gitee.com/gitee-fe/osui/tree/master/commits/b90c6e4f86dd65f3270afb3627fe7baf9811a615))
* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))
* 调整alert功能和icloud theme ([1b7d15b](https://gitee.com/gitee-fe/osui/tree/master/commits/1b7d15b741841378951a69d72db03ed334ab287e))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.5.6...@osui/icloud-theme@0.6.0) (2020-10-13)


### Bug Fixes

* modal icloud theme 调整 ([c8e0598](https://gitee.com/gitee-fe/osui/tree/master/commits/c8e059806876a3154c23b629008a9e27cae01caa))
* modal icloud theme 调整 ([7c56388](https://gitee.com/gitee-fe/osui/tree/master/commits/7c563884ea60f84489c56aa018d67a8e892fc5f4))
* pagniation icloud主题调整 ([1d1f513](https://gitee.com/gitee-fe/osui/tree/master/commits/1d1f5139658689663e4cdfcfd843cbcd6b654ba6))
* popover icloud theme调整 ([ceaa52a](https://gitee.com/gitee-fe/osui/tree/master/commits/ceaa52ac3f5057b8f644938dd5e722c2676827ef))
* radio button hover color icloud theme 调整 ([a8ed354](https://gitee.com/gitee-fe/osui/tree/master/commits/a8ed354377eea758c0e239cca0618a5d91c0e8d6))
* radio调整icloud主题 ([86a94a3](https://gitee.com/gitee-fe/osui/tree/master/commits/86a94a359ff0a03301df779f240a9e6beaa5933e))
* switch icloud主题样式调整 ([4cfd4c3](https://gitee.com/gitee-fe/osui/tree/master/commits/4cfd4c345a704f1f97f0453b7a921395c1164ccc))
* tabs icloud theme 调整 ([a0815ba](https://gitee.com/gitee-fe/osui/tree/master/commits/a0815baa516af2c799ad982538868ec6fed88d9c))
* 增加button default border高亮, success, error, warning 状态 ([32dfd0f](https://gitee.com/gitee-fe/osui/tree/master/commits/32dfd0f8ef987a3e0a3adc724f75c07f5d6c9a2a))
* 调整checkbox icloud主题样式 ([fd43d5b](https://gitee.com/gitee-fe/osui/tree/master/commits/fd43d5b6c846e19db8682b6b7ef70392844029f9))


### Features

* form icloud theme调整 ([b90c6e4](https://gitee.com/gitee-fe/osui/tree/master/commits/b90c6e4f86dd65f3270afb3627fe7baf9811a615))
* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))
* 调整alert功能和icloud theme ([1b7d15b](https://gitee.com/gitee-fe/osui/tree/master/commits/1b7d15b741841378951a69d72db03ed334ab287e))





## [0.5.7](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.5.6...@osui/icloud-theme@0.5.7) (2020-09-24)


### Features

* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))





## [0.5.6](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.5.5...@osui/icloud-theme@0.5.6) (2020-09-23)


### Bug Fixes

* 调整button icloud样式 ([924aef2](https://gitee.com/gitee-fe/osui/tree/master/commits/924aef2dec2fb1303f77d5fd032f73268b7dab7e))
* 调整button颜色实现 ([bc17800](https://gitee.com/gitee-fe/osui/tree/master/commits/bc178004aa4d80a5c3f4276556dd7a118cb0125d))
* 调整form,select,input focus,hover 样式 ([c54a8d0](https://gitee.com/gitee-fe/osui/tree/master/commits/c54a8d018d42e6a6b2c2f85826db8d9a53339ccc))





## [0.5.5](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.5.4...@osui/icloud-theme@0.5.5) (2020-09-22)


### Bug Fixes

* 图表色颜色调整 ([0b7a3db](https://gitee.com/gitee-fe/osui/tree/master/commits/0b7a3dbf6b7d5093788521e348f37874a8ca9252))





## [0.5.4](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.5.3...@osui/icloud-theme@0.5.4) (2020-09-21)


### Bug Fixes

* 调整button loading时的表现 ([91c0e35](https://gitee.com/gitee-fe/osui/tree/master/commits/91c0e354293dfd734a555931173f8a8715d97aa6))





## [0.5.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.5.2...@osui/icloud-theme@0.5.3) (2020-09-16)


### Bug Fixes

* icloud-theme placeholder color 修复 ([a8c3e0e](https://gitee.com/gitee-fe/osui/tree/master/commits/a8c3e0eada6eb20fed6c15149aea95da6f022e62))





## [0.5.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/icloud-theme@0.5.0...@osui/icloud-theme@0.5.2) (2020-09-14)


### Bug Fixes

* theme高度调整 ([4fe8ddc](https://gitee.com/gitee-fe/osui/tree/master/commits/4fe8ddc29e2eb649c6e21281803f0f2e5d3f7438))





# 0.5.0 (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))


### Features

* icloud-theme调整 ([bf7b7bb](https://gitee.com/gitee-fe/osui/tree/master/commits/bf7b7bb19b3b442273af9df94258492b684d0920))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/theme@0.4.2...@osui/theme@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/theme





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/theme@0.3.2...@osui/theme@0.4.2) (2020-09-07)


### Bug Fixes

* alert style ([82c0201](https://gitee.com/gitee-fe/osui/tree/master/commits/82c0201a2adb7bdb6d761dec95e6b3b38b317acf))
* avatar调整 ([dfde4ba](https://gitee.com/gitee-fe/osui/tree/master/commits/dfde4baa8f27f89c3246f7ea735cd05e2609c8a1))
* input border color ([7439d98](https://gitee.com/gitee-fe/osui/tree/master/commits/7439d98404bf6cddc114daa36c0b63fd79f1baf2))
* less-functions-override ([6f1f4c0](https://gitee.com/gitee-fe/osui/tree/master/commits/6f1f4c055f5701044f95a97ac3e7585a772e314a))
* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复alert样式 ([3db38e0](https://gitee.com/gitee-fe/osui/tree/master/commits/3db38e065d2f67673b98b6823bd6e93638096e36))
* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/tree/master/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 修复样式 ([9979e55](https://gitee.com/gitee-fe/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 修复样式 ([a43ed79](https://gitee.com/gitee-fe/osui/tree/master/commits/a43ed793f7b01e40526ba3b0917d8ac902ec2eb7))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 样式修复 ([4d6a435](https://gitee.com/gitee-fe/osui/tree/master/commits/4d6a435d8619434d977ea4988b2aa8474f90ce59))
* 解决icon冲突 ([297269b](https://gitee.com/gitee-fe/osui/tree/master/commits/297269bc9b980efba87e5759b82a4f5fbea04052))
* 解决冲突 ([d602c0e](https://gitee.com/gitee-fe/osui/tree/master/commits/d602c0ecaae95988d4c7d4b0c04e4dada9eb6e8f))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/tree/master/commits/b1eaed535b9704b14d463948f553940e78e48daa))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))


### Features

* 新增分页组件 ([c24133f](https://gitee.com/gitee-fe/osui/tree/master/commits/c24133f72fc5b3f00584e26f62daaa1a23083eac))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/theme@0.3.2...@osui/theme@0.4.0) (2020-09-01)


### Bug Fixes

* alert style ([82c0201](https://gitee.com/gitee-fe/osui/tree/master/commits/82c0201a2adb7bdb6d761dec95e6b3b38b317acf))
* input border color ([7439d98](https://gitee.com/gitee-fe/osui/tree/master/commits/7439d98404bf6cddc114daa36c0b63fd79f1baf2))
* less-functions-override ([6f1f4c0](https://gitee.com/gitee-fe/osui/tree/master/commits/6f1f4c055f5701044f95a97ac3e7585a772e314a))
* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复alert样式 ([3db38e0](https://gitee.com/gitee-fe/osui/tree/master/commits/3db38e065d2f67673b98b6823bd6e93638096e36))
* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/tree/master/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 修复样式 ([9979e55](https://gitee.com/gitee-fe/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 修复样式 ([a43ed79](https://gitee.com/gitee-fe/osui/tree/master/commits/a43ed793f7b01e40526ba3b0917d8ac902ec2eb7))
* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 解决icon冲突 ([297269b](https://gitee.com/gitee-fe/osui/tree/master/commits/297269bc9b980efba87e5759b82a4f5fbea04052))
* 解决冲突 ([d602c0e](https://gitee.com/gitee-fe/osui/tree/master/commits/d602c0ecaae95988d4c7d4b0c04e4dada9eb6e8f))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/tree/master/commits/b1eaed535b9704b14d463948f553940e78e48daa))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))


### Features

* 新增分页组件 ([c24133f](https://gitee.com/gitee-fe/osui/tree/master/commits/c24133f72fc5b3f00584e26f62daaa1a23083eac))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/theme@0.3.1...@osui/theme@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/theme





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/theme@0.3.0...@osui/theme@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/theme





# 0.3.0 (2020-08-17)


### Bug Fixes

* fix alert,input,timeline ([4f4911b](https://gitee.com/gitee-fe/osui/tree/master/commits/4f4911b2f580347e9933a10b0fcdeca3dd7c30d8))
* fix timeline,input,alert ([7a6034d](https://gitee.com/gitee-fe/osui/tree/master/commits/7a6034de9d4fd55317e65b1d61dbb12a6bff3a1e))
* **antd-vars-patch.less:** 删除ee的css ([354feb0](https://gitee.com/gitee-fe/osui/tree/master/commits/354feb0d5459b4763a48466147421e459318d520))
* **collapse&progress:** collapse&progress问题修改 ([7c0c692](https://gitee.com/gitee-fe/osui/tree/master/commits/7c0c6921ee99234df9f618b9200a9013623985e6))
* **icons:** 修复icons build时的问题 ([b725356](https://gitee.com/gitee-fe/osui/tree/master/commits/b725356c2d43d9d2e88b9fbb7b1208a51474a966))
* 修改公共样式文件注释，修改button命名 ([f585ce0](https://gitee.com/gitee-fe/osui/tree/master/commits/f585ce0c491838f8780d3dd262b9b429ea56a2f1))
* 解决antd-vats-path.less 冲突 ([d83d974](https://gitee.com/gitee-fe/osui/tree/master/commits/d83d9747bdd91258720814d47a4bcca352782069))
* 解决antd-vats-path.less 冲突 ([46b49b0](https://gitee.com/gitee-fe/osui/tree/master/commits/46b49b0f3d6a6867795639df37d0558209317267))


### Features

* **datepicker:** 新增日期组件 ([33a831d](https://gitee.com/gitee-fe/osui/tree/master/commits/33a831dee8c0e6f46eef039e1abe160c7147cbdc))
* **input,timeline,alert:** add input、timeline component,fix alert component ([f2d6e48](https://gitee.com/gitee-fe/osui/tree/master/commits/f2d6e48ecd05f411e6658ac43fa2df2452b60e1c))
* **theme:** 添加colors.js 脚本生成颜色 ([928acfd](https://gitee.com/gitee-fe/osui/tree/master/commits/928acfd65e19c8bc25804d44a9a8262170725148))
* 新增avatar，badge组件 ([f6db4a8](https://gitee.com/gitee-fe/osui/tree/master/commits/f6db4a8575c347ffe1aa3b1c575590ae8a844567))
* 新增Button组件 ([a19c3e0](https://gitee.com/gitee-fe/osui/tree/master/commits/a19c3e0e95a4e6644f35e302f437aba906e27726))
* 新增table组件 ([eb0b2e7](https://gitee.com/gitee-fe/osui/tree/master/commits/eb0b2e70c2a8fa16dc4f6d30fc90bdf3f0a0e004))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/tree/master/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
* 添加directory-navigator组件 ([593ea49](https://gitee.com/gitee-fe/osui/tree/master/commits/593ea499244b385bf89615f44e72d237ecd91607))





# [0.2.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/theme@0.1.2...@osui/theme@0.2.0) (2020-08-17)


### Bug Fixes

* fix alert,input,timeline ([4f4911b](https://gitee.com/gitee-fe/osui/tree/master/commits/4f4911b2f580347e9933a10b0fcdeca3dd7c30d8))
* fix timeline,input,alert ([7a6034d](https://gitee.com/gitee-fe/osui/tree/master/commits/7a6034de9d4fd55317e65b1d61dbb12a6bff3a1e))
* **antd-vars-patch.less:** 删除ee的css ([354feb0](https://gitee.com/gitee-fe/osui/tree/master/commits/354feb0d5459b4763a48466147421e459318d520))
* **collapse&progress:** collapse&progress问题修改 ([7c0c692](https://gitee.com/gitee-fe/osui/tree/master/commits/7c0c6921ee99234df9f618b9200a9013623985e6))
* **icons:** 修复icons build时的问题 ([b725356](https://gitee.com/gitee-fe/osui/tree/master/commits/b725356c2d43d9d2e88b9fbb7b1208a51474a966))
* 修改公共样式文件注释，修改button命名 ([f585ce0](https://gitee.com/gitee-fe/osui/tree/master/commits/f585ce0c491838f8780d3dd262b9b429ea56a2f1))
* 解决antd-vats-path.less 冲突 ([d83d974](https://gitee.com/gitee-fe/osui/tree/master/commits/d83d9747bdd91258720814d47a4bcca352782069))
* 解决antd-vats-path.less 冲突 ([46b49b0](https://gitee.com/gitee-fe/osui/tree/master/commits/46b49b0f3d6a6867795639df37d0558209317267))


### Features

* **datepicker:** 新增日期组件 ([33a831d](https://gitee.com/gitee-fe/osui/tree/master/commits/33a831dee8c0e6f46eef039e1abe160c7147cbdc))
* **input,timeline,alert:** add input、timeline component,fix alert component ([f2d6e48](https://gitee.com/gitee-fe/osui/tree/master/commits/f2d6e48ecd05f411e6658ac43fa2df2452b60e1c))
* 新增Button组件 ([a19c3e0](https://gitee.com/gitee-fe/osui/tree/master/commits/a19c3e0e95a4e6644f35e302f437aba906e27726))
* 新增table组件 ([eb0b2e7](https://gitee.com/gitee-fe/osui/tree/master/commits/eb0b2e70c2a8fa16dc4f6d30fc90bdf3f0a0e004))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/tree/master/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
* 添加directory-navigator组件 ([593ea49](https://gitee.com/gitee-fe/osui/tree/master/commits/593ea499244b385bf89615f44e72d237ecd91607))





## 0.1.2 (2020-08-07)


### Features

* **theme:** 添加colors.js 脚本生成颜色 ([928acfd](https://gitee.com/gitee-fe/osui/tree/master/commits/928acfd65e19c8bc25804d44a9a8262170725148))





## 0.0.1 (2020-08-07)

**Note:** Version bump only for package @osui/theme
