# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/gitee-fe/osui/compare/@osui/icons-builder@0.5.0...@osui/icons-builder@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/icons-builder





# [0.6.0](https://gitee.com/gitee-fe/osui/compare/@osui/icons-builder@0.5.0...@osui/icons-builder@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/icons-builder





# [0.5.0](https://gitee.com/gitee-fe/osui/compare/@osui/icons-builder@0.4.3...@osui/icons-builder@0.5.0) (2020-09-14)

**Note:** Version bump only for package @osui/icons-builder





## [0.4.3](https://gitee.com/gitee-fe/osui/compare/@osui/icons-builder@0.4.2...@osui/icons-builder@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/icons-builder





## [0.4.2](https://gitee.com/gitee-fe/osui/compare/@osui/icons-builder@0.3.0...@osui/icons-builder@0.4.2) (2020-09-07)

**Note:** Version bump only for package @osui/icons-builder





# [0.4.0](https://gitee.com/gitee-fe/osui/compare/@osui/icons-builder@0.3.0...@osui/icons-builder@0.4.0) (2020-09-01)

**Note:** Version bump only for package @osui/icons-builder





# 0.3.0 (2020-08-17)


### Bug Fixes

* **icons:** 修复icons build时的问题 ([b725356](https://gitee.com/gitee-fe/osui/commits/b725356c2d43d9d2e88b9fbb7b1208a51474a966))


### Features

* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
* 添加对colorful icon的支持 ([d6f1523](https://gitee.com/gitee-fe/osui/commits/d6f15230d7865e3017bfc6cf15c87367e3bb187e))





# 0.2.0 (2020-08-17)


### Bug Fixes

* **icons:** 修复icons build时的问题 ([b725356](https://gitee.com/gitee-fe/osui/commits/b725356c2d43d9d2e88b9fbb7b1208a51474a966))


### Features

* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
* 添加对colorful icon的支持 ([d6f1523](https://gitee.com/gitee-fe/osui/commits/d6f15230d7865e3017bfc6cf15c87367e3bb187e))
