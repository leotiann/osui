# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/gitee-fe/osui/compare/@osui/icons@0.5.0...@osui/icons@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/icons





# [0.6.0](https://gitee.com/gitee-fe/osui/compare/@osui/icons@0.5.0...@osui/icons@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/icons





# [0.5.0](https://gitee.com/gitee-fe/osui/compare/@osui/icons@0.4.3...@osui/icons@0.5.0) (2020-09-14)

**Note:** Version bump only for package @osui/icons





## [0.4.3](https://gitee.com/gitee-fe/osui/compare/@osui/icons@0.4.2...@osui/icons@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/icons





## [0.4.2](https://gitee.com/gitee-fe/osui/compare/@osui/icons@0.3.1...@osui/icons@0.4.2) (2020-09-07)


### Bug Fixes

* avatar调整 ([dfde4ba](https://gitee.com/gitee-fe/osui/commits/dfde4baa8f27f89c3246f7ea735cd05e2609c8a1))
* 修复alert样式 ([3db38e0](https://gitee.com/gitee-fe/osui/commits/3db38e065d2f67673b98b6823bd6e93638096e36))
* 修复collapse样式 ([f76ea21](https://gitee.com/gitee-fe/osui/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 修复message和alert代码调整 ([f0fbac7](https://gitee.com/gitee-fe/osui/commits/f0fbac740c0a124b7b2801cfffb70847f28fa74a))
* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 删除冲突代码 ([40908ac](https://gitee.com/gitee-fe/osui/commits/40908ac946afbe027927255f987f6be5c36d7b1b))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 解决icon冲突 ([297269b](https://gitee.com/gitee-fe/osui/commits/297269bc9b980efba87e5759b82a4f5fbea04052))
* 解决冲突 ([d602c0e](https://gitee.com/gitee-fe/osui/commits/d602c0ecaae95988d4c7d4b0c04e4dada9eb6e8f))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/gitee-fe/osui/commits/43014b7b6e860729398cd8ead30cd47953b76af7))
* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/commits/b1eaed535b9704b14d463948f553940e78e48daa))


### Features

* 新增branchDropdown组件 ([d1c904f](https://gitee.com/gitee-fe/osui/commits/d1c904fb8446093884cca573b88d5fed1290917f))
* 新增button-filter组件 ([59d8a33](https://gitee.com/gitee-fe/osui/commits/59d8a33c5febd1b2a835ab038c9eeb0948b24db8))
* 新增分页组件 ([c24133f](https://gitee.com/gitee-fe/osui/commits/c24133f72fc5b3f00584e26f62daaa1a23083eac))
* 添加form的调整 ([3a8cc63](https://gitee.com/gitee-fe/osui/commits/3a8cc63d9e06fde110cf641346064e81c572c67f))
* 添加select组件 ([d3ae6c4](https://gitee.com/gitee-fe/osui/commits/d3ae6c4da767a52f476b223f731fdcde20a4ebaf))





# [0.4.0](https://gitee.com/gitee-fe/osui/compare/@osui/icons@0.3.1...@osui/icons@0.4.0) (2020-09-01)


### Bug Fixes

* 修复alert样式 ([3db38e0](https://gitee.com/gitee-fe/osui/commits/3db38e065d2f67673b98b6823bd6e93638096e36))
* 修复collapse样式 ([f76ea21](https://gitee.com/gitee-fe/osui/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 修复message和alert代码调整 ([f0fbac7](https://gitee.com/gitee-fe/osui/commits/f0fbac740c0a124b7b2801cfffb70847f28fa74a))
* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 删除冲突代码 ([40908ac](https://gitee.com/gitee-fe/osui/commits/40908ac946afbe027927255f987f6be5c36d7b1b))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 解决icon冲突 ([297269b](https://gitee.com/gitee-fe/osui/commits/297269bc9b980efba87e5759b82a4f5fbea04052))
* 解决冲突 ([d602c0e](https://gitee.com/gitee-fe/osui/commits/d602c0ecaae95988d4c7d4b0c04e4dada9eb6e8f))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/gitee-fe/osui/commits/43014b7b6e860729398cd8ead30cd47953b76af7))
* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/commits/b1eaed535b9704b14d463948f553940e78e48daa))


### Features

* 新增branchDropdown组件 ([d1c904f](https://gitee.com/gitee-fe/osui/commits/d1c904fb8446093884cca573b88d5fed1290917f))
* 新增button-filter组件 ([59d8a33](https://gitee.com/gitee-fe/osui/commits/59d8a33c5febd1b2a835ab038c9eeb0948b24db8))
* 新增分页组件 ([c24133f](https://gitee.com/gitee-fe/osui/commits/c24133f72fc5b3f00584e26f62daaa1a23083eac))
* 添加form的调整 ([3a8cc63](https://gitee.com/gitee-fe/osui/commits/3a8cc63d9e06fde110cf641346064e81c572c67f))
* 添加select组件 ([d3ae6c4](https://gitee.com/gitee-fe/osui/commits/d3ae6c4da767a52f476b223f731fdcde20a4ebaf))





## [0.3.1](https://gitee.com/gitee-fe/osui/compare/@osui/icons@0.3.0...@osui/icons@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/icons





# 0.3.0 (2020-08-17)


### Bug Fixes

* **icons:** 修复icons build时的问题 ([b725356](https://gitee.com/gitee-fe/osui/commits/b725356c2d43d9d2e88b9fbb7b1208a51474a966))


### Features

* 新增avatar，badge组件 ([f6db4a8](https://gitee.com/gitee-fe/osui/commits/f6db4a8575c347ffe1aa3b1c575590ae8a844567))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
* 添加对colorful icon的支持 ([d6f1523](https://gitee.com/gitee-fe/osui/commits/d6f15230d7865e3017bfc6cf15c87367e3bb187e))





# 0.2.0 (2020-08-17)


### Bug Fixes

* **icons:** 修复icons build时的问题 ([b725356](https://gitee.com/gitee-fe/osui/commits/b725356c2d43d9d2e88b9fbb7b1208a51474a966))


### Features

* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
* 添加对colorful icon的支持 ([d6f1523](https://gitee.com/gitee-fe/osui/commits/d6f15230d7865e3017bfc6cf15c87367e3bb187e))
