# osui-icons

## Installation

```shell
npm i @osui/icons
```

```shell
yarn add @osui/icons
```

## Usage

```jsx
import { IconBackTop } from '@osui/icons'

export default function Title() {
  return (
    <div className="title">
      <h1>
        Hello Icons
        <IconBackTop style={{ color: '#999', marginLeft: 5 }} />
      </h1>
    </div>
  )
}
```

## Available Icons

{iconTable}
