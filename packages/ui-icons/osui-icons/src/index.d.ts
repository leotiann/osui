
interface IconProps extends React.SVGProps<SVGSVGElement> {
    scale?: number;
}
export const IconAdd: IconComponent;
export const IconBackTop: IconComponent;
export const IconBranchDropdown: IconComponent;
export const IconBranchFilter: IconComponent;
export const IconCheckCircleFill: IconComponent;
export const IconChecked: IconComponent;
export const IconCloseCircleFill: IconComponent;
export const IconCross: IconComponent;
export const IconDownArrow: IconComponent;
export const IconGitFilter: IconComponent;
export const IconHome: IconComponent;
export const IconInfoCircleFill: IconComponent;
export const IconLeftArrow: IconComponent;
export const IconRightArrow: IconComponent;
export const IconSearch: IconComponent;
export const IconSpinner: IconComponent;
export const IconSwitchCross: IconComponent;
export const IconSwitchTick: IconComponent;
export const IconWarningCircleFill: IconComponent;
export const IconNaviDoc: IconComponent;
