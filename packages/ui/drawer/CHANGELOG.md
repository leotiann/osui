# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.5.1...@osui/drawer@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/drawer





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.5.1...@osui/drawer@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/drawer





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.5.0...@osui/drawer@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/drawer





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.4.3...@osui/drawer@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.4.2...@osui/drawer@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/drawer





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.3.2...@osui/drawer@0.4.2) (2020-09-07)

**Note:** Version bump only for package @osui/drawer





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.3.2...@osui/drawer@0.4.0) (2020-09-01)

**Note:** Version bump only for package @osui/drawer





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.3.1...@osui/drawer@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/drawer





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/drawer@0.3.0...@osui/drawer@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/drawer





# 0.3.0 (2020-08-17)

**Note:** Version bump only for package @osui/drawer
