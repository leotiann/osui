# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.5.1...@osui/back-top@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/back-top





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.5.1...@osui/back-top@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/back-top





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.5.0...@osui/back-top@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/back-top





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.4.3...@osui/back-top@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.4.2...@osui/back-top@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/back-top





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.3.2...@osui/back-top@0.4.2) (2020-09-07)


### Bug Fixes

* back-top icon ([694588c](https://gitee.com/gitee-fe/osui/tree/master/commits/694588cbad1cb7adbaa33641d284df864299e2e8))
* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.3.2...@osui/back-top@0.4.0) (2020-09-01)


### Bug Fixes

* back-top icon ([694588c](https://gitee.com/gitee-fe/osui/tree/master/commits/694588cbad1cb7adbaa33641d284df864299e2e8))
* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.3.1...@osui/back-top@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/back-top





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/back-top@0.3.0...@osui/back-top@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/back-top





# 0.3.0 (2020-08-17)


### Bug Fixes

* 返回顶部组件代码 ([abd4c4c](https://gitee.com/gitee-fe/osui/tree/master/commits/abd4c4c0ee60e2c7f4e478b71cdf3341bbbdf5b8))


### Features

* back-top 新增悬浮状态 ([8f242bb](https://gitee.com/gitee-fe/osui/tree/master/commits/8f242bbdc35462bd7bbdd6faf8df05a79289212a))
* 新增返回顶部组件库 ([c042ac6](https://gitee.com/gitee-fe/osui/tree/master/commits/c042ac610bcec3ed618e07e28439708a8f63e205))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/tree/master/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))





# 0.2.0 (2020-08-17)


### Bug Fixes

* 返回顶部组件代码 ([abd4c4c](https://gitee.com/gitee-fe/osui/tree/master/commits/abd4c4c0ee60e2c7f4e478b71cdf3341bbbdf5b8))


### Features

* 新增返回顶部组件库 ([c042ac6](https://gitee.com/gitee-fe/osui/tree/master/commits/c042ac610bcec3ed618e07e28439708a8f63e205))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/tree/master/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
