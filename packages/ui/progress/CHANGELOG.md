# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.6.1...@osui/progress@0.6.2) (2020-10-21)

**Note:** Version bump only for package @osui/progress





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.5.1...@osui/progress@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/progress





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.5.1...@osui/progress@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/progress





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.5.0...@osui/progress@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/progress





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.4.3...@osui/progress@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.4.2...@osui/progress@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/progress





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.3.2...@osui/progress@0.4.2) (2020-09-07)

**Note:** Version bump only for package @osui/progress





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.3.2...@osui/progress@0.4.0) (2020-09-01)

**Note:** Version bump only for package @osui/progress





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.3.1...@osui/progress@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/progress





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/progress@0.3.0...@osui/progress@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/progress





# 0.3.0 (2020-08-17)


### Bug Fixes

* **collapse:** 折叠面板ghost默认值修改 ([a82d187](https://gitee.com/gitee-fe/osui/tree/master/commits/a82d1871c14013485cb60b8ba19d2370d7dce805))
* **collapse&progress:** collapse&progress问题修改 ([7c0c692](https://gitee.com/gitee-fe/osui/tree/master/commits/7c0c6921ee99234df9f618b9200a9013623985e6))
* **progress:** progress lint fix ([d183b83](https://gitee.com/gitee-fe/osui/tree/master/commits/d183b830490b86e8a64ca1e05f4b599a833e1f9e))


### Features

* **progress:** 新增progress组件 ([0ade9b7](https://gitee.com/gitee-fe/osui/tree/master/commits/0ade9b7a2f58bf4c0ba6e75af462bde5d6cb1196))





# 0.1.0 (2020-08-17)


### Bug Fixes

* **collapse:** 折叠面板ghost默认值修改 ([a82d187](https://gitee.com/gitee-fe/osui/tree/master/commits/a82d1871c14013485cb60b8ba19d2370d7dce805))
* **collapse&progress:** collapse&progress问题修改 ([7c0c692](https://gitee.com/gitee-fe/osui/tree/master/commits/7c0c6921ee99234df9f618b9200a9013623985e6))
* **progress:** progress lint fix ([d183b83](https://gitee.com/gitee-fe/osui/tree/master/commits/d183b830490b86e8a64ca1e05f4b599a833e1f9e))


### Features

* **progress:** 新增progress组件 ([0ade9b7](https://gitee.com/gitee-fe/osui/tree/master/commits/0ade9b7a2f58bf4c0ba6e75af462bde5d6cb1196))
