# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/form@0.6.2...@osui/form@0.6.3) (2020-10-20)


### Bug Fixes

* form代码调整 ([14a843f](https://gitee.com/gitee-fe/osui/tree/master/commits/14a843f9aacb56b4e0209aa0f490f6e1f7cb57eb))
* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/form@0.6.1...@osui/form@0.6.2) (2020-10-20)


### Bug Fixes

* form icloud-theme error message样式调整 ([3141f1b](https://gitee.com/gitee-fe/osui/tree/master/commits/3141f1bd9f34b1475eb9521566e466d35814a803))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/form@0.5.2...@osui/form@0.6.1) (2020-10-19)


### Bug Fixes

* form修复onChange没有透传的问题 ([28665aa](https://gitee.com/gitee-fe/osui/tree/master/commits/28665aac36d94950b218f94a8dd9e1edcf55da96))


### Features

* form icloud theme调整 ([b90c6e4](https://gitee.com/gitee-fe/osui/tree/master/commits/b90c6e4f86dd65f3270afb3627fe7baf9811a615))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/form@0.5.2...@osui/form@0.6.0) (2020-10-13)


### Features

* form icloud theme调整 ([b90c6e4](https://gitee.com/gitee-fe/osui/tree/master/commits/b90c6e4f86dd65f3270afb3627fe7baf9811a615))





## [0.5.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/form@0.5.1...@osui/form@0.5.2) (2020-09-23)


### Bug Fixes

* 调整form,select,input focus,hover 样式 ([c54a8d0](https://gitee.com/gitee-fe/osui/tree/master/commits/c54a8d018d42e6a6b2c2f85826db8d9a53339ccc))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/form@0.5.0...@osui/form@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/form





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/form@0.4.3...@osui/form@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/form@0.4.2...@osui/form@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/form





## 0.4.2 (2020-09-07)


### Bug Fixes

* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 样式修复 ([4d6a435](https://gitee.com/gitee-fe/osui/tree/master/commits/4d6a435d8619434d977ea4988b2aa8474f90ce59))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))


### Features

* 添加form的调整 ([3a8cc63](https://gitee.com/gitee-fe/osui/tree/master/commits/3a8cc63d9e06fde110cf641346064e81c572c67f))





# 0.4.0 (2020-09-01)


### Bug Fixes

* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))


### Features

* 添加form的调整 ([3a8cc63](https://gitee.com/gitee-fe/osui/tree/master/commits/3a8cc63d9e06fde110cf641346064e81c572c67f))
