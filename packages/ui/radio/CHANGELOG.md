# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.6.1...@osui/radio@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.5.1...@osui/radio@0.6.1) (2020-10-19)


### Bug Fixes

* radio调整icloud主题 ([86a94a3](https://gitee.com/gitee-fe/osui/tree/master/commits/86a94a359ff0a03301df779f240a9e6beaa5933e))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.5.1...@osui/radio@0.6.0) (2020-10-13)


### Bug Fixes

* radio调整icloud主题 ([86a94a3](https://gitee.com/gitee-fe/osui/tree/master/commits/86a94a359ff0a03301df779f240a9e6beaa5933e))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.5.0...@osui/radio@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/radio





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.4.3...@osui/radio@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.4.2...@osui/radio@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/radio





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.3.2...@osui/radio@0.4.2) (2020-09-07)


### Bug Fixes

* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/tree/master/commits/b1eaed535b9704b14d463948f553940e78e48daa))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.3.2...@osui/radio@0.4.0) (2020-09-01)


### Bug Fixes

* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/tree/master/commits/b1eaed535b9704b14d463948f553940e78e48daa))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.3.1...@osui/radio@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/radio





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/radio@0.3.0...@osui/radio@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/radio





# 0.3.0 (2020-08-17)


### Bug Fixes

* **checkboxgroup:** pr意见修改 ([4059b23](https://gitee.com/gitee-fe/osui/tree/master/commits/4059b23a1fa67c4458a6a5f4bfe600abd675b4fc))


### Features

* **radio:** 新增radio组件,修改checkbox lint ([f5b58aa](https://gitee.com/gitee-fe/osui/tree/master/commits/f5b58aab8b179caee6384dce7dd48de6b7a99021))
* **switch:** 新增switch组件 ([942201c](https://gitee.com/gitee-fe/osui/tree/master/commits/942201c054c18c3552a73c7baf01147cbc3a5b84))





# 0.1.0 (2020-08-17)


### Bug Fixes

* **checkboxgroup:** pr意见修改 ([4059b23](https://gitee.com/gitee-fe/osui/tree/master/commits/4059b23a1fa67c4458a6a5f4bfe600abd675b4fc))


### Features

* **radio:** 新增radio组件,修改checkbox lint ([f5b58aa](https://gitee.com/gitee-fe/osui/tree/master/commits/f5b58aab8b179caee6384dce7dd48de6b7a99021))
* **switch:** 新增switch组件 ([942201c](https://gitee.com/gitee-fe/osui/tree/master/commits/942201c054c18c3552a73c7baf01147cbc3a5b84))
