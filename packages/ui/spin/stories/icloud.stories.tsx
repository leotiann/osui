import {Spin} from 'antd';

export default {
    title: '待验收/Spin 加载中',
    component: Spin,
};

export const Demo = () => {
    return (
        <Spin />
    );
};
