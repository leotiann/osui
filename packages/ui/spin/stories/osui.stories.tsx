import {Spin} from 'antd';

export default {
    title: 'Spin',
    component: Spin,
};

export const Demo = () => {
    return (
        <Spin />
    );
};
