# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.6](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.6.5...@osui/ui@0.6.6) (2020-10-22)


### Bug Fixes

* @osui/ui package导出错误 ([b30b4e2](https://gitee.com/gitee-fe/osui/tree/master/commits/b30b4e22b2dc041520868e94c125ec72658273a2))





## [0.6.5](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.6.4...@osui/ui@0.6.5) (2020-10-21)


### Bug Fixes

* 新增Breadcrumb, Steps, Tree, Empty, AutoComplete, Cascader, Col, Dropdown, InputNumber, List, Menu, Row, Spon, TimePicker, TreeSelect 组件 ([2c4a9df](https://gitee.com/gitee-fe/osui/tree/master/commits/2c4a9df6af2a0283da7027a20043b0ccebceb2c4))





## [0.6.4](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.6.3...@osui/ui@0.6.4) (2020-10-20)

**Note:** Version bump only for package @osui/ui





## [0.6.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.6.2...@osui/ui@0.6.3) (2020-10-20)

**Note:** Version bump only for package @osui/ui





## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.5.5...@osui/ui@0.6.2) (2020-10-19)


### Features

* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.5.5...@osui/ui@0.6.0) (2020-10-13)


### Features

* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))





## [0.5.6](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.5.5...@osui/ui@0.5.6) (2020-09-24)


### Features

* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))





## [0.5.5](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.5.4...@osui/ui@0.5.5) (2020-09-23)

**Note:** Version bump only for package @osui/ui





## [0.5.4](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.5.3...@osui/ui@0.5.4) (2020-09-22)

**Note:** Version bump only for package @osui/ui





## [0.5.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.5.2...@osui/ui@0.5.3) (2020-09-21)

**Note:** Version bump only for package @osui/ui





## [0.5.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.5.1...@osui/ui@0.5.2) (2020-09-16)

**Note:** Version bump only for package @osui/ui





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.5.0...@osui/ui@0.5.1) (2020-09-16)

**Note:** Version bump only for package @osui/ui





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.4.4...@osui/ui@0.5.0) (2020-09-14)


### Bug Fixes

* 调整example，@osui/ui 修复导出命名出错的问题 ([ac26bce](https://gitee.com/gitee-fe/osui/tree/master/commits/ac26bcef52231792e77658c7a85e5694c53cb42e))





## [0.4.4](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.4.3...@osui/ui@0.4.4) (2020-09-08)


### Bug Fixes

* message导出名字错误 ([09d3959](https://gitee.com/gitee-fe/osui/tree/master/commits/09d3959a351e42a235c42432e1174bf987d0a036))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.4.2...@osui/ui@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/ui





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.3.2...@osui/ui@0.4.2) (2020-09-07)

**Note:** Version bump only for package @osui/ui





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.3.2...@osui/ui@0.4.0) (2020-09-01)

**Note:** Version bump only for package @osui/ui





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.3.1...@osui/ui@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/ui





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/ui@0.3.0...@osui/ui@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/ui





# 0.3.0 (2020-08-17)


### Features

* **ui:** ui包导出所有组件 ([5ef63db](https://gitee.com/gitee-fe/osui/tree/master/commits/5ef63db89de00ff92a703e9e99d5e6aa1ed3bfde))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/tree/master/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))





# 0.2.0 (2020-08-17)


### Features

* **ui:** ui包导出所有组件 ([5ef63db](https://gitee.com/gitee-fe/osui/tree/master/commits/5ef63db89de00ff92a703e9e99d5e6aa1ed3bfde))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/tree/master/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
