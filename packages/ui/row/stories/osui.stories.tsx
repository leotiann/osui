import {Row} from 'antd';

export default {
    title: 'Row',
    component: Row,
};

export const Demo = () => {
    return (
        <Row />
    );
};
