# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.6.1...@osui/modal@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.5.3...@osui/modal@0.6.1) (2020-10-19)


### Bug Fixes

* modal icloud theme 调整 ([c8e0598](https://gitee.com/gitee-fe/osui/tree/master/commits/c8e059806876a3154c23b629008a9e27cae01caa))
* modal icloud theme 调整 ([7c56388](https://gitee.com/gitee-fe/osui/tree/master/commits/7c563884ea60f84489c56aa018d67a8e892fc5f4))
* modal, switch icloud-theme样式修复 ([d1eea7d](https://gitee.com/gitee-fe/osui/tree/master/commits/d1eea7d5984887a7bec2fd8fa59dcf867b6564c4))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.5.3...@osui/modal@0.6.0) (2020-10-13)


### Bug Fixes

* modal icloud theme 调整 ([c8e0598](https://gitee.com/gitee-fe/osui/tree/master/commits/c8e059806876a3154c23b629008a9e27cae01caa))
* modal icloud theme 调整 ([7c56388](https://gitee.com/gitee-fe/osui/tree/master/commits/7c563884ea60f84489c56aa018d67a8e892fc5f4))





## [0.5.4](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.5.3...@osui/modal@0.5.4) (2020-09-24)

**Note:** Version bump only for package @osui/modal





## [0.5.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.5.2...@osui/modal@0.5.3) (2020-09-23)

**Note:** Version bump only for package @osui/modal





## [0.5.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.5.1...@osui/modal@0.5.2) (2020-09-22)

**Note:** Version bump only for package @osui/modal





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.5.0...@osui/modal@0.5.1) (2020-09-21)

**Note:** Version bump only for package @osui/modal





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.4.3...@osui/modal@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.4.2...@osui/modal@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/modal





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.3.2...@osui/modal@0.4.2) (2020-09-07)


### Bug Fixes

* 样式修复 ([4d6a435](https://gitee.com/gitee-fe/osui/tree/master/commits/4d6a435d8619434d977ea4988b2aa8474f90ce59))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/gitee-fe/osui/tree/master/commits/43014b7b6e860729398cd8ead30cd47953b76af7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.3.2...@osui/modal@0.4.0) (2020-09-01)


### Bug Fixes

* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/gitee-fe/osui/tree/master/commits/43014b7b6e860729398cd8ead30cd47953b76af7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.3.1...@osui/modal@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/modal





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/modal@0.3.0...@osui/modal@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/modal





# 0.3.0 (2020-08-17)


### Bug Fixes

* **modal:** 修改Modal api模式 ([40852fc](https://gitee.com/gitee-fe/osui/tree/master/commits/40852fc0014d0e330dc3df0492d1eb26dca59a6e))


### Features

* **modal:** 新增modal组件 ([768209d](https://gitee.com/gitee-fe/osui/tree/master/commits/768209d0307f553e3ecab54d51a70dba98230239))
* **modal:** 新增modal组件 ([8cd8e4a](https://gitee.com/gitee-fe/osui/tree/master/commits/8cd8e4ae2de4373ae6c0caf932c66e8a089af10c))





# 0.1.0 (2020-08-17)


### Bug Fixes

* **modal:** 修改Modal api模式 ([40852fc](https://gitee.com/gitee-fe/osui/tree/master/commits/40852fc0014d0e330dc3df0492d1eb26dca59a6e))


### Features

* **modal:** 新增modal组件 ([768209d](https://gitee.com/gitee-fe/osui/tree/master/commits/768209d0307f553e3ecab54d51a70dba98230239))
* **modal:** 新增modal组件 ([8cd8e4a](https://gitee.com/gitee-fe/osui/tree/master/commits/8cd8e4ae2de4373ae6c0caf932c66e8a089af10c))
