# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.6.1...@osui/input@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.5.2...@osui/input@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/input





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.5.2...@osui/input@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/input





## [0.5.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.5.1...@osui/input@0.5.2) (2020-09-23)


### Bug Fixes

* 调整form,select,input focus,hover 样式 ([c54a8d0](https://gitee.com/gitee-fe/osui/tree/master/commits/c54a8d018d42e6a6b2c2f85826db8d9a53339ccc))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.5.0...@osui/input@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/input





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.4.3...@osui/input@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.4.2...@osui/input@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/input





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.3.2...@osui/input@0.4.2) (2020-09-07)


### Bug Fixes

* input border color ([7439d98](https://gitee.com/gitee-fe/osui/tree/master/commits/7439d98404bf6cddc114daa36c0b63fd79f1baf2))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/tree/master/commits/b1eaed535b9704b14d463948f553940e78e48daa))


### Features

* 添加form的调整 ([3a8cc63](https://gitee.com/gitee-fe/osui/tree/master/commits/3a8cc63d9e06fde110cf641346064e81c572c67f))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.3.2...@osui/input@0.4.0) (2020-09-01)


### Bug Fixes

* input border color ([7439d98](https://gitee.com/gitee-fe/osui/tree/master/commits/7439d98404bf6cddc114daa36c0b63fd79f1baf2))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/tree/master/commits/b1eaed535b9704b14d463948f553940e78e48daa))


### Features

* 添加form的调整 ([3a8cc63](https://gitee.com/gitee-fe/osui/tree/master/commits/3a8cc63d9e06fde110cf641346064e81c572c67f))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.3.1...@osui/input@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/input





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/input@0.3.0...@osui/input@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/input





# 0.3.0 (2020-08-17)


### Bug Fixes

* fix alert,input,timeline ([4f4911b](https://gitee.com/gitee-fe/osui/tree/master/commits/4f4911b2f580347e9933a10b0fcdeca3dd7c30d8))
* fix timeline,input,alert ([1643140](https://gitee.com/gitee-fe/osui/tree/master/commits/164314038b39511bc3a82fda0b621c9d7c6209fa))
* fix timeline,input,alert ([7a6034d](https://gitee.com/gitee-fe/osui/tree/master/commits/7a6034de9d4fd55317e65b1d61dbb12a6bff3a1e))
* 修改css文件和格式 ([08b7615](https://gitee.com/gitee-fe/osui/tree/master/commits/08b7615a386c6d0c0d62884bd51a94887cf12825))


### Features

* **input,timeline,alert:** add input、timeline component,fix alert component ([f2d6e48](https://gitee.com/gitee-fe/osui/tree/master/commits/f2d6e48ecd05f411e6658ac43fa2df2452b60e1c))





# 0.2.0 (2020-08-17)


### Bug Fixes

* fix alert,input,timeline ([4f4911b](https://gitee.com/gitee-fe/osui/tree/master/commits/4f4911b2f580347e9933a10b0fcdeca3dd7c30d8))
* fix timeline,input,alert ([1643140](https://gitee.com/gitee-fe/osui/tree/master/commits/164314038b39511bc3a82fda0b621c9d7c6209fa))
* fix timeline,input,alert ([7a6034d](https://gitee.com/gitee-fe/osui/tree/master/commits/7a6034de9d4fd55317e65b1d61dbb12a6bff3a1e))
* 修改css文件和格式 ([08b7615](https://gitee.com/gitee-fe/osui/tree/master/commits/08b7615a386c6d0c0d62884bd51a94887cf12825))


### Features

* **input,timeline,alert:** add input、timeline component,fix alert component ([f2d6e48](https://gitee.com/gitee-fe/osui/tree/master/commits/f2d6e48ecd05f411e6658ac43fa2df2452b60e1c))
