import {Dropdown} from 'antd';

export default {
    title: 'Dropdown',
    component: Dropdown,
};

export const Demo = () => {
    return (
        <Dropdown />
    );
};
