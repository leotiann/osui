# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/toggle-button@0.5.1...@osui/toggle-button@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/toggle-button





# [0.6.0](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/toggle-button@0.5.1...@osui/toggle-button@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/toggle-button





## [0.5.1](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/toggle-button@0.5.0...@osui/toggle-button@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/toggle-button





# [0.5.0](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/toggle-button@0.4.3...@osui/toggle-button@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/yuxuanhuo/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/toggle-button@0.4.2...@osui/toggle-button@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/toggle-button





## 0.4.2 (2020-09-07)


### Bug Fixes

* 修复collapse样式 ([f76ea21](https://gitee.com/yuxuanhuo/osui/tree/master/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 样式修复 ([896665a](https://gitee.com/yuxuanhuo/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))





# 0.4.0 (2020-09-01)


### Bug Fixes

* 修复collapse样式 ([f76ea21](https://gitee.com/yuxuanhuo/osui/tree/master/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 样式修复 ([896665a](https://gitee.com/yuxuanhuo/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
