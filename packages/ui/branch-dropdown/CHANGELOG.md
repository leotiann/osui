# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.6.1...@osui/branch-dropdown@0.6.2) (2020-10-20)

**Note:** Version bump only for package @osui/branch-dropdown





## [0.6.1](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.5.3...@osui/branch-dropdown@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/branch-dropdown





# [0.6.0](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.5.3...@osui/branch-dropdown@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/branch-dropdown





## [0.5.4](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.5.3...@osui/branch-dropdown@0.5.4) (2020-09-24)

**Note:** Version bump only for package @osui/branch-dropdown





## [0.5.3](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.5.2...@osui/branch-dropdown@0.5.3) (2020-09-23)

**Note:** Version bump only for package @osui/branch-dropdown





## [0.5.2](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.5.1...@osui/branch-dropdown@0.5.2) (2020-09-22)

**Note:** Version bump only for package @osui/branch-dropdown





## [0.5.1](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.5.0...@osui/branch-dropdown@0.5.1) (2020-09-21)

**Note:** Version bump only for package @osui/branch-dropdown





# [0.5.0](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.4.3...@osui/branch-dropdown@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/yuxuanhuo/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/branch-dropdown@0.4.2...@osui/branch-dropdown@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/branch-dropdown





## 0.4.2 (2020-09-07)


### Bug Fixes

* 优化branch-dropdown代码，更新了传参的定义 ([4325129](https://gitee.com/yuxuanhuo/osui/tree/master/commits/43251292c638a41107c913e7baa36c9f5ede25e3))
* 修复collapse样式 ([f76ea21](https://gitee.com/yuxuanhuo/osui/tree/master/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 修复样式 ([9979e55](https://gitee.com/yuxuanhuo/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 修改branch-dropdown的readme文件 ([948edf6](https://gitee.com/yuxuanhuo/osui/tree/master/commits/948edf61e83c41948cbb0b9de67cf2aded53a00d))
* 样式修复 ([896665a](https://gitee.com/yuxuanhuo/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/yuxuanhuo/osui/tree/master/commits/43014b7b6e860729398cd8ead30cd47953b76af7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/yuxuanhuo/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))


### Features

* 新增branchDropdown组件 ([d1c904f](https://gitee.com/yuxuanhuo/osui/tree/master/commits/d1c904fb8446093884cca573b88d5fed1290917f))





# 0.4.0 (2020-09-01)


### Bug Fixes

* 优化branch-dropdown代码，更新了传参的定义 ([4325129](https://gitee.com/yuxuanhuo/osui/tree/master/commits/43251292c638a41107c913e7baa36c9f5ede25e3))
* 修复collapse样式 ([f76ea21](https://gitee.com/yuxuanhuo/osui/tree/master/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 修复样式 ([9979e55](https://gitee.com/yuxuanhuo/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 修改branch-dropdown的readme文件 ([948edf6](https://gitee.com/yuxuanhuo/osui/tree/master/commits/948edf61e83c41948cbb0b9de67cf2aded53a00d))
* 样式修复 ([896665a](https://gitee.com/yuxuanhuo/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/yuxuanhuo/osui/tree/master/commits/43014b7b6e860729398cd8ead30cd47953b76af7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/yuxuanhuo/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))


### Features

* 新增branchDropdown组件 ([d1c904f](https://gitee.com/yuxuanhuo/osui/tree/master/commits/d1c904fb8446093884cca573b88d5fed1290917f))
