# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.6.1...@osui/collapse@0.6.2) (2020-10-21)

**Note:** Version bump only for package @osui/collapse





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.5.1...@osui/collapse@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/collapse





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.5.1...@osui/collapse@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/collapse





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.5.0...@osui/collapse@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/collapse





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.4.3...@osui/collapse@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.4.2...@osui/collapse@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/collapse





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.3.2...@osui/collapse@0.4.2) (2020-09-07)


### Bug Fixes

* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复collapse样式 ([f76ea21](https://gitee.com/gitee-fe/osui/tree/master/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.3.2...@osui/collapse@0.4.0) (2020-09-01)


### Bug Fixes

* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复collapse样式 ([f76ea21](https://gitee.com/gitee-fe/osui/tree/master/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.3.1...@osui/collapse@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/collapse





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/collapse@0.3.0...@osui/collapse@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/collapse





# 0.3.0 (2020-08-17)


### Bug Fixes

* **collapse:** 折叠面板ghost默认值修改 ([a82d187](https://gitee.com/gitee-fe/osui/tree/master/commits/a82d1871c14013485cb60b8ba19d2370d7dce805))
* **collapse&progress:** collapse&progress问题修改 ([7c0c692](https://gitee.com/gitee-fe/osui/tree/master/commits/7c0c6921ee99234df9f618b9200a9013623985e6))


### Features

* **collapse:** 新增collapse组件 ([4b69b4b](https://gitee.com/gitee-fe/osui/tree/master/commits/4b69b4bd0aaa9cbfd778657b7a207c104f990f35))





# 0.1.0 (2020-08-17)


### Bug Fixes

* **collapse:** 折叠面板ghost默认值修改 ([a82d187](https://gitee.com/gitee-fe/osui/tree/master/commits/a82d1871c14013485cb60b8ba19d2370d7dce805))
* **collapse&progress:** collapse&progress问题修改 ([7c0c692](https://gitee.com/gitee-fe/osui/tree/master/commits/7c0c6921ee99234df9f618b9200a9013623985e6))


### Features

* **collapse:** 新增collapse组件 ([4b69b4b](https://gitee.com/gitee-fe/osui/tree/master/commits/4b69b4bd0aaa9cbfd778657b7a207c104f990f35))
