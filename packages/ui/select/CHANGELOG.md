# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.6.1...@osui/select@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.5.4...@osui/select@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/select





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.5.4...@osui/select@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/select





## [0.5.4](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.5.3...@osui/select@0.5.4) (2020-09-23)


### Bug Fixes

* 调整form,select,input focus,hover 样式 ([c54a8d0](https://gitee.com/gitee-fe/osui/tree/master/commits/c54a8d018d42e6a6b2c2f85826db8d9a53339ccc))





## [0.5.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.5.2...@osui/select@0.5.3) (2020-09-22)

**Note:** Version bump only for package @osui/select





## [0.5.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.5.1...@osui/select@0.5.2) (2020-09-16)


### Bug Fixes

* 调整select multiple checkbox大小 ([e5dd7ce](https://gitee.com/gitee-fe/osui/tree/master/commits/e5dd7ce69c50fc76cc2a04e928df9f4e3ca44306))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.5.0...@osui/select@0.5.1) (2020-09-16)


### Bug Fixes

* ant-select 多选样式调整 ([70338df](https://gitee.com/gitee-fe/osui/tree/master/commits/70338df143036b459cc8b4f530d7909c13ceece1))





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.4.3...@osui/select@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/select@0.4.2...@osui/select@0.4.3) (2020-09-08)


### Bug Fixes

* 修复multiselect dropdown classname没有透传下去的问题 ([d4964e4](https://gitee.com/gitee-fe/osui/tree/master/commits/d4964e4c6f4fb50fe331c61491b5a785ba6e3cdc))





## 0.4.2 (2020-09-07)


### Bug Fixes

* input border color ([7439d98](https://gitee.com/gitee-fe/osui/tree/master/commits/7439d98404bf6cddc114daa36c0b63fd79f1baf2))
* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复collapse样式 ([f76ea21](https://gitee.com/gitee-fe/osui/tree/master/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 修复样式 ([9979e55](https://gitee.com/gitee-fe/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 样式修复 ([4d6a435](https://gitee.com/gitee-fe/osui/tree/master/commits/4d6a435d8619434d977ea4988b2aa8474f90ce59))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))


### Features

* 添加select组件 ([d3ae6c4](https://gitee.com/gitee-fe/osui/tree/master/commits/d3ae6c4da767a52f476b223f731fdcde20a4ebaf))





# 0.4.0 (2020-09-01)


### Bug Fixes

* input border color ([7439d98](https://gitee.com/gitee-fe/osui/tree/master/commits/7439d98404bf6cddc114daa36c0b63fd79f1baf2))
* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复collapse样式 ([f76ea21](https://gitee.com/gitee-fe/osui/tree/master/commits/f76ea21bc8046c3265d2554bc1aed20698041219))
* 修复样式 ([9979e55](https://gitee.com/gitee-fe/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))


### Features

* 添加select组件 ([d3ae6c4](https://gitee.com/gitee-fe/osui/tree/master/commits/d3ae6c4da767a52f476b223f731fdcde20a4ebaf))
