import {Cascader} from 'antd';

export default {
    title: 'Cascader',
    component: Cascader,
};

export const Demo = () => {
    return (
        <Cascader />
    );
};
