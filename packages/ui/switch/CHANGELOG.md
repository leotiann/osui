# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.6.1...@osui/switch@0.6.2) (2020-10-21)

**Note:** Version bump only for package @osui/switch





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.5.1...@osui/switch@0.6.1) (2020-10-19)


### Bug Fixes

* modal, switch icloud-theme样式修复 ([d1eea7d](https://gitee.com/gitee-fe/osui/tree/master/commits/d1eea7d5984887a7bec2fd8fa59dcf867b6564c4))
* switch icloud主题样式调整 ([4cfd4c3](https://gitee.com/gitee-fe/osui/tree/master/commits/4cfd4c345a704f1f97f0453b7a921395c1164ccc))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.5.1...@osui/switch@0.6.0) (2020-10-13)


### Bug Fixes

* switch icloud主题样式调整 ([4cfd4c3](https://gitee.com/gitee-fe/osui/tree/master/commits/4cfd4c345a704f1f97f0453b7a921395c1164ccc))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.5.0...@osui/switch@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/switch





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.4.3...@osui/switch@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.4.2...@osui/switch@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/switch





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.3.2...@osui/switch@0.4.2) (2020-09-07)


### Bug Fixes

* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/tree/master/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 样式修复 ([4d6a435](https://gitee.com/gitee-fe/osui/tree/master/commits/4d6a435d8619434d977ea4988b2aa8474f90ce59))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.3.2...@osui/switch@0.4.0) (2020-09-01)


### Bug Fixes

* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/tree/master/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.3.1...@osui/switch@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/switch





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/switch@0.3.0...@osui/switch@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/switch





# 0.3.0 (2020-08-17)


### Features

* **switch:** 新增switch组件 ([942201c](https://gitee.com/gitee-fe/osui/tree/master/commits/942201c054c18c3552a73c7baf01147cbc3a5b84))





# 0.1.0 (2020-08-17)


### Features

* **switch:** 新增switch组件 ([942201c](https://gitee.com/gitee-fe/osui/tree/master/commits/942201c054c18c3552a73c7baf01147cbc3a5b84))
