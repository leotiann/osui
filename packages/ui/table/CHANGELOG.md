# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.6.1...@osui/table@0.6.2) (2020-10-21)


### Bug Fixes

* table icloud-theme样式修复 ([3f0d5e3](https://gitee.com/gitee-fe/osui/tree/master/commits/3f0d5e36838640cf65145b5f489dfbb629f0ac8f))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.5.1...@osui/table@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/table





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.5.1...@osui/table@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/table





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.5.0...@osui/table@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/table





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.4.3...@osui/table@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.4.2...@osui/table@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/table





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.3.2...@osui/table@0.4.2) (2020-09-07)


### Bug Fixes

* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/tree/master/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 解决icon冲突 ([297269b](https://gitee.com/gitee-fe/osui/tree/master/commits/297269bc9b980efba87e5759b82a4f5fbea04052))
* 解决冲突 ([d602c0e](https://gitee.com/gitee-fe/osui/tree/master/commits/d602c0ecaae95988d4c7d4b0c04e4dada9eb6e8f))


### Features

* 新增分页组件 ([c24133f](https://gitee.com/gitee-fe/osui/tree/master/commits/c24133f72fc5b3f00584e26f62daaa1a23083eac))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.3.2...@osui/table@0.4.0) (2020-09-01)


### Bug Fixes

* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/tree/master/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 解决icon冲突 ([297269b](https://gitee.com/gitee-fe/osui/tree/master/commits/297269bc9b980efba87e5759b82a4f5fbea04052))
* 解决冲突 ([d602c0e](https://gitee.com/gitee-fe/osui/tree/master/commits/d602c0ecaae95988d4c7d4b0c04e4dada9eb6e8f))


### Features

* 新增分页组件 ([c24133f](https://gitee.com/gitee-fe/osui/tree/master/commits/c24133f72fc5b3f00584e26f62daaa1a23083eac))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.3.1...@osui/table@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/table





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/table@0.3.0...@osui/table@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/table





# 0.3.0 (2020-08-17)


### Features

* 新增table组件 ([eb0b2e7](https://gitee.com/gitee-fe/osui/tree/master/commits/eb0b2e70c2a8fa16dc4f6d30fc90bdf3f0a0e004))





# 0.2.0 (2020-08-17)


### Features

* 新增table组件 ([eb0b2e7](https://gitee.com/gitee-fe/osui/tree/master/commits/eb0b2e70c2a8fa16dc4f6d30fc90bdf3f0a0e004))
