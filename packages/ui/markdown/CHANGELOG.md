# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.5.1...@osui/markdown@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/markdown





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.5.1...@osui/markdown@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/markdown





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.5.0...@osui/markdown@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/markdown





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.4.3...@osui/markdown@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.4.2...@osui/markdown@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/markdown





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.3.2...@osui/markdown@0.4.2) (2020-09-07)


### Bug Fixes

* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 修复样式 ([a43ed79](https://gitee.com/gitee-fe/osui/tree/master/commits/a43ed793f7b01e40526ba3b0917d8ac902ec2eb7))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.3.2...@osui/markdown@0.4.0) (2020-09-01)


### Bug Fixes

* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 修复样式 ([a43ed79](https://gitee.com/gitee-fe/osui/tree/master/commits/a43ed793f7b01e40526ba3b0917d8ac902ec2eb7))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.3.1...@osui/markdown@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/markdown





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/markdown@0.3.0...@osui/markdown@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/markdown





# 0.3.0 (2020-08-17)


### Features

* **markdown:** 添加markdown组件 ([b8e5cf8](https://gitee.com/gitee-fe/osui/tree/master/commits/b8e5cf81c78ecc732eaae9e88452cd9611432843))





# 0.2.0 (2020-08-17)


### Features

* **markdown:** 添加markdown组件 ([b8e5cf8](https://gitee.com/gitee-fe/osui/tree/master/commits/b8e5cf81c78ecc732eaae9e88452cd9611432843))
