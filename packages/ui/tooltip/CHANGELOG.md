# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.5.1...@osui/tooltip@0.6.1) (2020-10-19)


### Features

* 调整alert功能和icloud theme ([1b7d15b](https://gitee.com/gitee-fe/osui/tree/master/commits/1b7d15b741841378951a69d72db03ed334ab287e))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.5.1...@osui/tooltip@0.6.0) (2020-10-13)


### Features

* 调整alert功能和icloud theme ([1b7d15b](https://gitee.com/gitee-fe/osui/tree/master/commits/1b7d15b741841378951a69d72db03ed334ab287e))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.5.0...@osui/tooltip@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/tooltip





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.4.3...@osui/tooltip@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.4.2...@osui/tooltip@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/tooltip





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.3.2...@osui/tooltip@0.4.2) (2020-09-07)


### Bug Fixes

* less-functions-override ([6f1f4c0](https://gitee.com/gitee-fe/osui/tree/master/commits/6f1f4c055f5701044f95a97ac3e7585a772e314a))
* 修复样式 ([9979e55](https://gitee.com/gitee-fe/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/gitee-fe/osui/tree/master/commits/43014b7b6e860729398cd8ead30cd47953b76af7))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.3.2...@osui/tooltip@0.4.0) (2020-09-01)


### Bug Fixes

* less-functions-override ([6f1f4c0](https://gitee.com/gitee-fe/osui/tree/master/commits/6f1f4c055f5701044f95a97ac3e7585a772e314a))
* 修复样式 ([9979e55](https://gitee.com/gitee-fe/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/gitee-fe/osui/tree/master/commits/43014b7b6e860729398cd8ead30cd47953b76af7))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.3.1...@osui/tooltip@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/tooltip





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/tooltip@0.3.0...@osui/tooltip@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/tooltip





# 0.3.0 (2020-08-17)

**Note:** Version bump only for package @osui/tooltip
