# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.6.1...@osui/timeline@0.6.2) (2020-10-21)

**Note:** Version bump only for package @osui/timeline





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.5.1...@osui/timeline@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/timeline





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.5.1...@osui/timeline@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/timeline





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.5.0...@osui/timeline@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/timeline





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.4.3...@osui/timeline@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.4.2...@osui/timeline@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/timeline





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.3.2...@osui/timeline@0.4.2) (2020-09-07)


### Bug Fixes

* 修复样式 ([a43ed79](https://gitee.com/gitee-fe/osui/tree/master/commits/a43ed793f7b01e40526ba3b0917d8ac902ec2eb7))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.3.2...@osui/timeline@0.4.0) (2020-09-01)


### Bug Fixes

* 修复样式 ([a43ed79](https://gitee.com/gitee-fe/osui/tree/master/commits/a43ed793f7b01e40526ba3b0917d8ac902ec2eb7))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.3.1...@osui/timeline@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/timeline





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/timeline@0.3.0...@osui/timeline@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/timeline





# 0.3.0 (2020-08-17)


### Bug Fixes

* fix alert,input,timeline ([4f4911b](https://gitee.com/gitee-fe/osui/tree/master/commits/4f4911b2f580347e9933a10b0fcdeca3dd7c30d8))
* fix timeline,input,alert ([7a6034d](https://gitee.com/gitee-fe/osui/tree/master/commits/7a6034de9d4fd55317e65b1d61dbb12a6bff3a1e))
* 修改css和doc文件 ([be019d3](https://gitee.com/gitee-fe/osui/tree/master/commits/be019d3bf734df67cd3cde9a7cfccabf3aa5fd2a))
* 修改css文件和格式 ([08b7615](https://gitee.com/gitee-fe/osui/tree/master/commits/08b7615a386c6d0c0d62884bd51a94887cf12825))


### Features

* **input,timeline,alert:** add input、timeline component,fix alert component ([f2d6e48](https://gitee.com/gitee-fe/osui/tree/master/commits/f2d6e48ecd05f411e6658ac43fa2df2452b60e1c))





# 0.2.0 (2020-08-17)


### Bug Fixes

* fix alert,input,timeline ([4f4911b](https://gitee.com/gitee-fe/osui/tree/master/commits/4f4911b2f580347e9933a10b0fcdeca3dd7c30d8))
* fix timeline,input,alert ([7a6034d](https://gitee.com/gitee-fe/osui/tree/master/commits/7a6034de9d4fd55317e65b1d61dbb12a6bff3a1e))
* 修改css和doc文件 ([be019d3](https://gitee.com/gitee-fe/osui/tree/master/commits/be019d3bf734df67cd3cde9a7cfccabf3aa5fd2a))
* 修改css文件和格式 ([08b7615](https://gitee.com/gitee-fe/osui/tree/master/commits/08b7615a386c6d0c0d62884bd51a94887cf12825))


### Features

* **input,timeline,alert:** add input、timeline component,fix alert component ([f2d6e48](https://gitee.com/gitee-fe/osui/tree/master/commits/f2d6e48ecd05f411e6658ac43fa2df2452b60e1c))
