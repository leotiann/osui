# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.6.2...@osui/message@0.6.3) (2020-10-21)


### Bug Fixes

* message closeicon 位置调整 ([4a7601b](https://gitee.com/gitee-fe/osui/tree/master/commits/4a7601b6249904d74ca649269ed9051eb3fb91bb))





## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.6.1...@osui/message@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.5.1...@osui/message@0.6.1) (2020-10-19)


### Bug Fixes

* 调整message icloud theme ([45e0e6a](https://gitee.com/gitee-fe/osui/tree/master/commits/45e0e6ac48b01f0a5c1f2a6a9fabb6192d0ee937))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.5.1...@osui/message@0.6.0) (2020-10-13)


### Bug Fixes

* 调整message icloud theme ([45e0e6a](https://gitee.com/gitee-fe/osui/tree/master/commits/45e0e6ac48b01f0a5c1f2a6a9fabb6192d0ee937))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.5.0...@osui/message@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/message





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.4.3...@osui/message@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.4.2...@osui/message@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/message





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.3.2...@osui/message@0.4.2) (2020-09-07)


### Bug Fixes

* 修复message和alert代码调整 ([f0fbac7](https://gitee.com/gitee-fe/osui/tree/master/commits/f0fbac740c0a124b7b2801cfffb70847f28fa74a))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.3.2...@osui/message@0.4.0) (2020-09-01)


### Bug Fixes

* 修复message和alert代码调整 ([f0fbac7](https://gitee.com/gitee-fe/osui/tree/master/commits/f0fbac740c0a124b7b2801cfffb70847f28fa74a))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.3.1...@osui/message@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/message





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/message@0.3.0...@osui/message@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/message





# 0.3.0 (2020-08-17)


### Bug Fixes

* **message:** message组件props默认值修改 ([258658e](https://gitee.com/gitee-fe/osui/tree/master/commits/258658e64c477371216f8d01e114eb632b3a7f2d))
* message单独引入lodash.partial ([f1bb806](https://gitee.com/gitee-fe/osui/tree/master/commits/f1bb806df8756692941059af6fc445d70a6a36e1))
* 调整message ([173bd04](https://gitee.com/gitee-fe/osui/tree/master/commits/173bd0407040bfc9f00758b53fe76d8b0a7be8e3))


### Features

* **message:** 新增message组件 ([6507e80](https://gitee.com/gitee-fe/osui/tree/master/commits/6507e8000aeb1671b4fbfbfb41269e79fcfbc95e))





# 0.1.0 (2020-08-17)


### Bug Fixes

* **message:** message组件props默认值修改 ([258658e](https://gitee.com/gitee-fe/osui/tree/master/commits/258658e64c477371216f8d01e114eb632b3a7f2d))
* message单独引入lodash.partial ([f1bb806](https://gitee.com/gitee-fe/osui/tree/master/commits/f1bb806df8756692941059af6fc445d70a6a36e1))
* 调整message ([173bd04](https://gitee.com/gitee-fe/osui/tree/master/commits/173bd0407040bfc9f00758b53fe76d8b0a7be8e3))


### Features

* **message:** 新增message组件 ([6507e80](https://gitee.com/gitee-fe/osui/tree/master/commits/6507e8000aeb1671b4fbfbfb41269e79fcfbc95e))
