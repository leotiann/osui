# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.6.1...@osui/button@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.5.3...@osui/button@0.6.1) (2020-10-19)


### Bug Fixes

* 增加button default border高亮, success, error, warning 状态 ([32dfd0f](https://gitee.com/gitee-fe/osui/tree/master/commits/32dfd0f8ef987a3e0a3adc724f75c07f5d6c9a2a))


### Features

* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.5.3...@osui/button@0.6.0) (2020-10-13)


### Bug Fixes

* 增加button default border高亮, success, error, warning 状态 ([32dfd0f](https://gitee.com/gitee-fe/osui/tree/master/commits/32dfd0f8ef987a3e0a3adc724f75c07f5d6c9a2a))


### Features

* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))





## [0.5.4](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.5.3...@osui/button@0.5.4) (2020-09-24)


### Features

* 增加Space组件, 增加button group形式的demo ([39eaa10](https://gitee.com/gitee-fe/osui/tree/master/commits/39eaa104cda0bd352b1a2c791856db98e673a7fb))





## [0.5.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.5.2...@osui/button@0.5.3) (2020-09-23)


### Bug Fixes

* 调整button icloud样式 ([924aef2](https://gitee.com/gitee-fe/osui/tree/master/commits/924aef2dec2fb1303f77d5fd032f73268b7dab7e))
* 调整button颜色实现 ([bc17800](https://gitee.com/gitee-fe/osui/tree/master/commits/bc178004aa4d80a5c3f4276556dd7a118cb0125d))





## [0.5.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.5.1...@osui/button@0.5.2) (2020-09-22)

**Note:** Version bump only for package @osui/button





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.5.0...@osui/button@0.5.1) (2020-09-21)


### Bug Fixes

* 调整button loading时的表现 ([91c0e35](https://gitee.com/gitee-fe/osui/tree/master/commits/91c0e354293dfd734a555931173f8a8715d97aa6))





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.4.3...@osui/button@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))


### Features

* icloud-theme调整 ([bf7b7bb](https://gitee.com/gitee-fe/osui/tree/master/commits/bf7b7bb19b3b442273af9df94258492b684d0920))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.4.2...@osui/button@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/button





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.3.2...@osui/button@0.4.2) (2020-09-07)


### Bug Fixes

* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 修复样式 ([9979e55](https://gitee.com/gitee-fe/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.3.2...@osui/button@0.4.0) (2020-09-01)


### Bug Fixes

* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 修复样式 ([9979e55](https://gitee.com/gitee-fe/osui/tree/master/commits/9979e556048898e5182851e9cfde7f464b1c749d))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.3.1...@osui/button@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/button





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/button@0.3.0...@osui/button@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/button





# 0.3.0 (2020-08-17)


### Bug Fixes

* 修改公共样式文件注释，修改button命名 ([f585ce0](https://gitee.com/gitee-fe/osui/tree/master/commits/f585ce0c491838f8780d3dd262b9b429ea56a2f1))


### Features

* 新增Button组件 ([a19c3e0](https://gitee.com/gitee-fe/osui/tree/master/commits/a19c3e0e95a4e6644f35e302f437aba906e27726))





# 0.2.0 (2020-08-17)


### Bug Fixes

* 修改公共样式文件注释，修改button命名 ([f585ce0](https://gitee.com/gitee-fe/osui/tree/master/commits/f585ce0c491838f8780d3dd262b9b429ea56a2f1))


### Features

* 新增Button组件 ([a19c3e0](https://gitee.com/gitee-fe/osui/tree/master/commits/a19c3e0e95a4e6644f35e302f437aba906e27726))
