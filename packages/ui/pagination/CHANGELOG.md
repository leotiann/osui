# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/pagination@0.6.1...@osui/pagination@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/yuxuanhuo/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/pagination@0.5.1...@osui/pagination@0.6.1) (2020-10-19)


### Bug Fixes

* pagniation icloud主题调整 ([1d1f513](https://gitee.com/yuxuanhuo/osui/tree/master/commits/1d1f5139658689663e4cdfcfd843cbcd6b654ba6))





# [0.6.0](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/pagination@0.5.1...@osui/pagination@0.6.0) (2020-10-13)


### Bug Fixes

* pagniation icloud主题调整 ([1d1f513](https://gitee.com/yuxuanhuo/osui/tree/master/commits/1d1f5139658689663e4cdfcfd843cbcd6b654ba6))





## [0.5.1](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/pagination@0.5.0...@osui/pagination@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/pagination





# [0.5.0](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/pagination@0.4.3...@osui/pagination@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/yuxuanhuo/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/yuxuanhuo/osui/tree/master/compare/@osui/pagination@0.4.2...@osui/pagination@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/pagination





## 0.4.2 (2020-09-07)


### Bug Fixes

* 样式修复 ([896665a](https://gitee.com/yuxuanhuo/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))


### Features

* 新增分页组件 ([39f929e](https://gitee.com/yuxuanhuo/osui/tree/master/commits/39f929e643f4954664aa4da926e5897e76225e39))
* 新增分页组件 ([c24133f](https://gitee.com/yuxuanhuo/osui/tree/master/commits/c24133f72fc5b3f00584e26f62daaa1a23083eac))





# 0.4.0 (2020-09-01)


### Bug Fixes

* 样式修复 ([896665a](https://gitee.com/yuxuanhuo/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))


### Features

* 新增分页组件 ([39f929e](https://gitee.com/yuxuanhuo/osui/tree/master/commits/39f929e643f4954664aa4da926e5897e76225e39))
* 新增分页组件 ([c24133f](https://gitee.com/yuxuanhuo/osui/tree/master/commits/c24133f72fc5b3f00584e26f62daaa1a23083eac))
