# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.6.1...@osui/checkbox@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.5.1...@osui/checkbox@0.6.1) (2020-10-19)


### Bug Fixes

* 调整checkbox icloud主题样式 ([fd43d5b](https://gitee.com/gitee-fe/osui/tree/master/commits/fd43d5b6c846e19db8682b6b7ef70392844029f9))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.5.1...@osui/checkbox@0.6.0) (2020-10-13)


### Bug Fixes

* 调整checkbox icloud主题样式 ([fd43d5b](https://gitee.com/gitee-fe/osui/tree/master/commits/fd43d5b6c846e19db8682b6b7ef70392844029f9))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.5.0...@osui/checkbox@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/checkbox





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.4.3...@osui/checkbox@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))


### Features

* icloud-theme调整 ([bf7b7bb](https://gitee.com/gitee-fe/osui/tree/master/commits/bf7b7bb19b3b442273af9df94258492b684d0920))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.4.2...@osui/checkbox@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/checkbox





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.3.2...@osui/checkbox@0.4.2) (2020-09-07)


### Bug Fixes

* 样式修复 ([4d6a435](https://gitee.com/gitee-fe/osui/tree/master/commits/4d6a435d8619434d977ea4988b2aa8474f90ce59))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.3.2...@osui/checkbox@0.4.0) (2020-09-01)


### Bug Fixes

* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.3.1...@osui/checkbox@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/checkbox





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/checkbox@0.3.0...@osui/checkbox@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/checkbox





# 0.3.0 (2020-08-17)


### Bug Fixes

* **checkbox:** 导出checkboxGroup ([cce680f](https://gitee.com/gitee-fe/osui/tree/master/commits/cce680f7ee1fa9570e690b2e457b49424f769e4a))
* **checkbox:** 导出checkboxGroup ([1a76fd8](https://gitee.com/gitee-fe/osui/tree/master/commits/1a76fd801d2d10b496e65d5229bfb7530c49a9e9))
* **checkboxgroup:** pr意见修改 ([4059b23](https://gitee.com/gitee-fe/osui/tree/master/commits/4059b23a1fa67c4458a6a5f4bfe600abd675b4fc))


### Features

* **checkbox:** 新增checkbox组件 ([a62ed41](https://gitee.com/gitee-fe/osui/tree/master/commits/a62ed4120967c10d1c15ec0775ac2ffc68f810fd))
* **radio:** 新增radio组件,修改checkbox lint ([f5b58aa](https://gitee.com/gitee-fe/osui/tree/master/commits/f5b58aab8b179caee6384dce7dd48de6b7a99021))





# 0.1.0 (2020-08-17)


### Bug Fixes

* **checkbox:** 导出checkboxGroup ([cce680f](https://gitee.com/gitee-fe/osui/tree/master/commits/cce680f7ee1fa9570e690b2e457b49424f769e4a))
* **checkbox:** 导出checkboxGroup ([1a76fd8](https://gitee.com/gitee-fe/osui/tree/master/commits/1a76fd801d2d10b496e65d5229bfb7530c49a9e9))
* **checkboxgroup:** pr意见修改 ([4059b23](https://gitee.com/gitee-fe/osui/tree/master/commits/4059b23a1fa67c4458a6a5f4bfe600abd675b4fc))


### Features

* **checkbox:** 新增checkbox组件 ([a62ed41](https://gitee.com/gitee-fe/osui/tree/master/commits/a62ed4120967c10d1c15ec0775ac2ffc68f810fd))
* **radio:** 新增radio组件,修改checkbox lint ([f5b58aa](https://gitee.com/gitee-fe/osui/tree/master/commits/f5b58aab8b179caee6384dce7dd48de6b7a99021))
