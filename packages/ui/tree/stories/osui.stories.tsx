import {Tree} from 'antd';

export default {
    title: 'Tree',
    component: Tree,
};

export const Demo = () => {
    return (
        <Tree />
    );
};
