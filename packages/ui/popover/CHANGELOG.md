# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/popover@0.6.1...@osui/popover@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/popover@0.5.1...@osui/popover@0.6.1) (2020-10-19)


### Bug Fixes

* popover icloud theme调整 ([ceaa52a](https://gitee.com/gitee-fe/osui/tree/master/commits/ceaa52ac3f5057b8f644938dd5e722c2676827ef))
* 修复popover overlayClassName透传问题 ([77d16db](https://gitee.com/gitee-fe/osui/tree/master/commits/77d16dba740ccb19eb95ebcecb5fadc8b070ce77))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/popover@0.5.1...@osui/popover@0.6.0) (2020-10-13)


### Bug Fixes

* popover icloud theme调整 ([ceaa52a](https://gitee.com/gitee-fe/osui/tree/master/commits/ceaa52ac3f5057b8f644938dd5e722c2676827ef))
* 修复popover overlayClassName透传问题 ([77d16db](https://gitee.com/gitee-fe/osui/tree/master/commits/77d16dba740ccb19eb95ebcecb5fadc8b070ce77))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/popover@0.5.0...@osui/popover@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/popover





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/popover@0.4.3...@osui/popover@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/popover@0.4.2...@osui/popover@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/popover





## 0.4.2 (2020-09-07)


### Bug Fixes

* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))


### Features

* 新增popover组件 ([7b83a7c](https://gitee.com/gitee-fe/osui/tree/master/commits/7b83a7caa0c3003a1a8cf2bd495a8b07eb731104))





# 0.4.0 (2020-09-01)


### Bug Fixes

* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))


### Features

* 新增popover组件 ([7b83a7c](https://gitee.com/gitee-fe/osui/tree/master/commits/7b83a7caa0c3003a1a8cf2bd495a8b07eb731104))
