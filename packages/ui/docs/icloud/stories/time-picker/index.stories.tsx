import {TimePicker} from 'antd';

export default {
    title: '待验收/TimePicker 时间选择框',
    component: TimePicker,
};

export const Demo = () => {
    return (
        <TimePicker />
    );
};
