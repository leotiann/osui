import {Tree} from 'antd';

export default {
    title: '待验收/Tree 树形组件',
    component: Tree,
};

export const Demo = () => {
    return (
        <Tree />
    );
};
