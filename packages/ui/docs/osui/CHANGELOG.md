# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/osui-docs@0.5.2...@osui/osui-docs@0.6.1) (2020-10-19)


### Bug Fixes

* 增加button default border高亮, success, error, warning 状态 ([32dfd0f](https://gitee.com/gitee-fe/osui/tree/master/commits/32dfd0f8ef987a3e0a3adc724f75c07f5d6c9a2a))
* 调整button icloud样式 ([924aef2](https://gitee.com/gitee-fe/osui/tree/master/commits/924aef2dec2fb1303f77d5fd032f73268b7dab7e))
* 调整form,select,input focus,hover 样式 ([c54a8d0](https://gitee.com/gitee-fe/osui/tree/master/commits/c54a8d018d42e6a6b2c2f85826db8d9a53339ccc))
* 调整message icloud theme ([45e0e6a](https://gitee.com/gitee-fe/osui/tree/master/commits/45e0e6ac48b01f0a5c1f2a6a9fabb6192d0ee937))


### Features

* 调整alert功能和icloud theme ([1b7d15b](https://gitee.com/gitee-fe/osui/tree/master/commits/1b7d15b741841378951a69d72db03ed334ab287e))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/osui-docs@0.5.2...@osui/osui-docs@0.6.0) (2020-10-13)


### Bug Fixes

* 增加button default border高亮, success, error, warning 状态 ([32dfd0f](https://gitee.com/gitee-fe/osui/tree/master/commits/32dfd0f8ef987a3e0a3adc724f75c07f5d6c9a2a))
* 调整button icloud样式 ([924aef2](https://gitee.com/gitee-fe/osui/tree/master/commits/924aef2dec2fb1303f77d5fd032f73268b7dab7e))
* 调整form,select,input focus,hover 样式 ([c54a8d0](https://gitee.com/gitee-fe/osui/tree/master/commits/c54a8d018d42e6a6b2c2f85826db8d9a53339ccc))
* 调整message icloud theme ([45e0e6a](https://gitee.com/gitee-fe/osui/tree/master/commits/45e0e6ac48b01f0a5c1f2a6a9fabb6192d0ee937))


### Features

* 调整alert功能和icloud theme ([1b7d15b](https://gitee.com/gitee-fe/osui/tree/master/commits/1b7d15b741841378951a69d72db03ed334ab287e))





## 0.5.2 (2020-09-16)

**Note:** Version bump only for package @osui/osui-docs





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.5.0...@osui/docs@0.5.1) (2020-09-14)


### Bug Fixes

* theme高度调整 ([4fe8ddc](https://gitee.com/gitee-fe/osui/tree/master/commits/4fe8ddc29e2eb649c6e21281803f0f2e5d3f7438))





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.4.3...@osui/docs@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))


### Features

* icloud-theme调整 ([bf7b7bb](https://gitee.com/gitee-fe/osui/tree/master/commits/bf7b7bb19b3b442273af9df94258492b684d0920))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.4.2...@osui/docs@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/docs





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.3.1...@osui/docs@0.4.2) (2020-09-07)


### Bug Fixes

* avatar调整 ([dfde4ba](https://gitee.com/gitee-fe/osui/tree/master/commits/dfde4baa8f27f89c3246f7ea735cd05e2609c8a1))
* input border color ([7439d98](https://gitee.com/gitee-fe/osui/tree/master/commits/7439d98404bf6cddc114daa36c0b63fd79f1baf2))
* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/tree/master/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/gitee-fe/osui/tree/master/commits/43014b7b6e860729398cd8ead30cd47953b76af7))
* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/tree/master/commits/b1eaed535b9704b14d463948f553940e78e48daa))


### Features

* 添加form的调整 ([3a8cc63](https://gitee.com/gitee-fe/osui/tree/master/commits/3a8cc63d9e06fde110cf641346064e81c572c67f))
* 添加select组件 ([d3ae6c4](https://gitee.com/gitee-fe/osui/tree/master/commits/d3ae6c4da767a52f476b223f731fdcde20a4ebaf))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.3.1...@osui/docs@0.4.0) (2020-09-01)


### Bug Fixes

* input border color ([7439d98](https://gitee.com/gitee-fe/osui/tree/master/commits/7439d98404bf6cddc114daa36c0b63fd79f1baf2))
* 修复table switch ([29c9ba8](https://gitee.com/gitee-fe/osui/tree/master/commits/29c9ba8b79904566ea84e010dd5ffa62c8e75ab4))
* 修复样式 ([94c5450](https://gitee.com/gitee-fe/osui/tree/master/commits/94c545078c2b4c05dee48b880f32bed2d11459ea))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))
* 调整menu-dropdpwn; 添加文档 ([43014b7](https://gitee.com/gitee-fe/osui/tree/master/commits/43014b7b6e860729398cd8ead30cd47953b76af7))
* 调整radio和input的search icon ([b1eaed5](https://gitee.com/gitee-fe/osui/tree/master/commits/b1eaed535b9704b14d463948f553940e78e48daa))


### Features

* 添加form的调整 ([3a8cc63](https://gitee.com/gitee-fe/osui/tree/master/commits/3a8cc63d9e06fde110cf641346064e81c572c67f))
* 添加select组件 ([d3ae6c4](https://gitee.com/gitee-fe/osui/tree/master/commits/d3ae6c4da767a52f476b223f731fdcde20a4ebaf))





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.3.0...@osui/docs@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/docs





# [0.3.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.1.1...@osui/docs@0.3.0) (2020-08-17)


### Bug Fixes

* **alert:** alert文档完善, 修复alert样式 ([caba747](https://gitee.com/gitee-fe/osui/tree/master/commits/caba747e4d1c100a759bf908c951e78fd553b09f))


### Features

* **markdown:** 添加markdown组件 ([b8e5cf8](https://gitee.com/gitee-fe/osui/tree/master/commits/b8e5cf81c78ecc732eaae9e88452cd9611432843))





# [0.2.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.1.2...@osui/docs@0.2.0) (2020-08-17)


### Features

* **markdown:** 添加markdown组件 ([b8e5cf8](https://gitee.com/gitee-fe/osui/tree/master/commits/b8e5cf81c78ecc732eaae9e88452cd9611432843))





## [0.1.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/docs@0.1.1...@osui/docs@0.1.2) (2020-08-07)


### Bug Fixes

* **alert:** alert文档完善, 修复alert样式 ([caba747](https://gitee.com/gitee-fe/osui/tree/master/commits/caba747e4d1c100a759bf908c951e78fd553b09f))





## 0.1.1 (2020-08-07)

**Note:** Version bump only for package @osui/docs
