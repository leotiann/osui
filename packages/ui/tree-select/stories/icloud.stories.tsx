import {TreeSelect} from 'antd';

export default {
    title: '待验收/TreeSelect 树选择',
    component: TreeSelect,
};

export const Demo = () => {
    return (
        <TreeSelect />
    );
};
