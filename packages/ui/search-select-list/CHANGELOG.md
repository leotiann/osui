# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.5.1...@osui/search-select-list@0.6.1) (2020-10-19)

**Note:** Version bump only for package @osui/search-select-list





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.5.1...@osui/search-select-list@0.6.0) (2020-10-13)

**Note:** Version bump only for package @osui/search-select-list





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.5.0...@osui/search-select-list@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/search-select-list





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.4.3...@osui/search-select-list@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.4.2...@osui/search-select-list@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/search-select-list





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.3.2...@osui/search-select-list@0.4.2) (2020-09-07)


### Bug Fixes

* 修复样式 ([a43ed79](https://gitee.com/gitee-fe/osui/tree/master/commits/a43ed793f7b01e40526ba3b0917d8ac902ec2eb7))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.3.2...@osui/search-select-list@0.4.0) (2020-09-01)


### Bug Fixes

* 修复样式 ([a43ed79](https://gitee.com/gitee-fe/osui/tree/master/commits/a43ed793f7b01e40526ba3b0917d8ac902ec2eb7))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 调整Button,popover,search-select-list的样式 ([aa66478](https://gitee.com/gitee-fe/osui/tree/master/commits/aa66478c36f1b271c008b8ce71e923f218b68fb7))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.3.1...@osui/search-select-list@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/search-select-list





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/search-select-list@0.3.0...@osui/search-select-list@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/search-select-list





# 0.3.0 (2020-08-17)


### Features

* **select-search-list:** 新增select-search-list组件 ([dfb8652](https://gitee.com/gitee-fe/osui/tree/master/commits/dfb8652356a64fd13c17559d040cd6b35ae773bc))





# 0.2.0 (2020-08-17)


### Features

* **select-search-list:** 新增select-search-list组件 ([dfb8652](https://gitee.com/gitee-fe/osui/tree/master/commits/dfb8652356a64fd13c17559d040cd6b35ae773bc))
