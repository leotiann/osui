import {Breadcrumb} from 'antd';

export default {
    title: 'Breadcrumb',
    component: Breadcrumb,
};

export const Demo = () => {
    return (
        <Breadcrumb />
    );
};
