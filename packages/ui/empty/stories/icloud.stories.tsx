import {Empty} from 'antd';

export default {
    title: '待验收/Empty 空状态',
    component: Empty,
};

export const Demo = () => {
    return (
        <Empty />
    );
};
