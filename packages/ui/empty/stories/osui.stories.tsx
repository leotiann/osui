import {Empty} from 'antd';

export default {
    title: 'Empty',
    component: Empty,
};

export const Demo = () => {
    return (
        <Empty />
    );
};
