# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.6.1...@osui/alert@0.6.2) (2020-10-20)


### Bug Fixes

* popover padding; chore：默认开发icloud-theme ([356cc34](https://gitee.com/gitee-fe/osui/tree/master/commits/356cc34992918ffb6c3d30283b2810fed8e18e79))





## [0.6.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.5.1...@osui/alert@0.6.1) (2020-10-19)


### Bug Fixes

* 调整message icloud theme ([45e0e6a](https://gitee.com/gitee-fe/osui/tree/master/commits/45e0e6ac48b01f0a5c1f2a6a9fabb6192d0ee937))


### Features

* 调整alert功能和icloud theme ([1b7d15b](https://gitee.com/gitee-fe/osui/tree/master/commits/1b7d15b741841378951a69d72db03ed334ab287e))





# [0.6.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.5.1...@osui/alert@0.6.0) (2020-10-13)


### Bug Fixes

* 调整message icloud theme ([45e0e6a](https://gitee.com/gitee-fe/osui/tree/master/commits/45e0e6ac48b01f0a5c1f2a6a9fabb6192d0ee937))


### Features

* 调整alert功能和icloud theme ([1b7d15b](https://gitee.com/gitee-fe/osui/tree/master/commits/1b7d15b741841378951a69d72db03ed334ab287e))





## [0.5.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.5.0...@osui/alert@0.5.1) (2020-09-22)

**Note:** Version bump only for package @osui/alert





# [0.5.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.4.3...@osui/alert@0.5.0) (2020-09-14)


### Bug Fixes

* 文档title调整; icloud-theme调整; 组件size调整 ([7453a43](https://gitee.com/gitee-fe/osui/tree/master/commits/7453a437fb419db875709b32f934ba9e3454f895))


### Features

* icloud-theme调整 ([bf7b7bb](https://gitee.com/gitee-fe/osui/tree/master/commits/bf7b7bb19b3b442273af9df94258492b684d0920))





## [0.4.3](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.4.2...@osui/alert@0.4.3) (2020-09-08)

**Note:** Version bump only for package @osui/alert





## [0.4.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.3.2...@osui/alert@0.4.2) (2020-09-07)


### Bug Fixes

* alert style ([82c0201](https://gitee.com/gitee-fe/osui/tree/master/commits/82c0201a2adb7bdb6d761dec95e6b3b38b317acf))
* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复alert样式 ([3db38e0](https://gitee.com/gitee-fe/osui/tree/master/commits/3db38e065d2f67673b98b6823bd6e93638096e36))
* 修复message和alert代码调整 ([f0fbac7](https://gitee.com/gitee-fe/osui/tree/master/commits/f0fbac740c0a124b7b2801cfffb70847f28fa74a))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





# [0.4.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.3.2...@osui/alert@0.4.0) (2020-09-01)


### Bug Fixes

* alert style ([82c0201](https://gitee.com/gitee-fe/osui/tree/master/commits/82c0201a2adb7bdb6d761dec95e6b3b38b317acf))
* ue样式修复 ([b96ad12](https://gitee.com/gitee-fe/osui/tree/master/commits/b96ad1267689dd649f0a8bf82bedcbeff7e60983))
* 修复alert样式 ([3db38e0](https://gitee.com/gitee-fe/osui/tree/master/commits/3db38e065d2f67673b98b6823bd6e93638096e36))
* 修复message和alert代码调整 ([f0fbac7](https://gitee.com/gitee-fe/osui/tree/master/commits/f0fbac740c0a124b7b2801cfffb70847f28fa74a))
* 样式修复 ([896665a](https://gitee.com/gitee-fe/osui/tree/master/commits/896665a45f52be9a2896157f20125f8a77809e34))
* 颜色调整, radio调整 ([385419f](https://gitee.com/gitee-fe/osui/tree/master/commits/385419f7bad6483fcef158f6afce33b846d084b9))





## [0.3.2](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.3.1...@osui/alert@0.3.2) (2020-08-17)

**Note:** Version bump only for package @osui/alert





## [0.3.1](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.3.0...@osui/alert@0.3.1) (2020-08-17)

**Note:** Version bump only for package @osui/alert





# [0.3.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.1.1...@osui/alert@0.3.0) (2020-08-17)


### Bug Fixes

* **alert:** alert文档完善, 修复alert样式 ([caba747](https://gitee.com/gitee-fe/osui/tree/master/commits/caba747e4d1c100a759bf908c951e78fd553b09f))
* **alert:** 调整alert storybook引入 ([ab9036c](https://gitee.com/gitee-fe/osui/tree/master/commits/ab9036c43e3f46428d7676527fe066349de804b8))
* fix alert,input,timeline ([4f4911b](https://gitee.com/gitee-fe/osui/tree/master/commits/4f4911b2f580347e9933a10b0fcdeca3dd7c30d8))
* fix timeline,input,alert ([7a6034d](https://gitee.com/gitee-fe/osui/tree/master/commits/7a6034de9d4fd55317e65b1d61dbb12a6bff3a1e))
* 修改css和doc文件 ([be019d3](https://gitee.com/gitee-fe/osui/tree/master/commits/be019d3bf734df67cd3cde9a7cfccabf3aa5fd2a))
* 修改css文件和格式 ([08b7615](https://gitee.com/gitee-fe/osui/tree/master/commits/08b7615a386c6d0c0d62884bd51a94887cf12825))


### Features

* **input,timeline,alert:** add input、timeline component,fix alert component ([f2d6e48](https://gitee.com/gitee-fe/osui/tree/master/commits/f2d6e48ecd05f411e6658ac43fa2df2452b60e1c))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/tree/master/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))





# [0.2.0](https://gitee.com/gitee-fe/osui/tree/master/compare/@osui/alert@0.1.2...@osui/alert@0.2.0) (2020-08-17)


### Bug Fixes

* **alert:** 调整alert storybook引入 ([ab9036c](https://gitee.com/gitee-fe/osui/tree/master/commits/ab9036c43e3f46428d7676527fe066349de804b8))
* fix alert,input,timeline ([4f4911b](https://gitee.com/gitee-fe/osui/tree/master/commits/4f4911b2f580347e9933a10b0fcdeca3dd7c30d8))
* fix timeline,input,alert ([7a6034d](https://gitee.com/gitee-fe/osui/tree/master/commits/7a6034de9d4fd55317e65b1d61dbb12a6bff3a1e))
* 修改css和doc文件 ([be019d3](https://gitee.com/gitee-fe/osui/tree/master/commits/be019d3bf734df67cd3cde9a7cfccabf3aa5fd2a))
* 修改css文件和格式 ([08b7615](https://gitee.com/gitee-fe/osui/tree/master/commits/08b7615a386c6d0c0d62884bd51a94887cf12825))


### Features

* **input,timeline,alert:** add input、timeline component,fix alert component ([f2d6e48](https://gitee.com/gitee-fe/osui/tree/master/commits/f2d6e48ecd05f411e6658ac43fa2df2452b60e1c))
* 添加@osui/icons, 并在back-top组件中使用 ([28092fa](https://gitee.com/gitee-fe/osui/tree/master/commits/28092fa3d54a91b116ffe5fc05f43a628fc376c0))
